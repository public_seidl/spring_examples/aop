package sk.seidl.spring_aop.repositories;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import sk.seidl.spring_aop.anotations.TractTime;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Slf4j
@Repository
public class Dao1 {
    @TractTime
    public String retrieveSomething(){
        //log.info("DAO 1: retrieveSomething" );
        return " Dao1 ";
    }
}
