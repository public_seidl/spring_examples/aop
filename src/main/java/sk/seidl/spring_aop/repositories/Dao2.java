package sk.seidl.spring_aop.repositories;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Slf4j
@Repository
public class Dao2 {
    public String retrieveSomething(){
       // log.info("DAO 2: retrieveSomething" );
        return " Dao2 ";
    }
}
