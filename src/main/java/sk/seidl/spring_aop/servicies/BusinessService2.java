package sk.seidl.spring_aop.servicies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.seidl.spring_aop.repositories.Dao2;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Service
public class BusinessService2 {
    @Autowired
    private Dao2 dao;

    public String calculateSomething() {
        return dao.retrieveSomething();
    }
}
