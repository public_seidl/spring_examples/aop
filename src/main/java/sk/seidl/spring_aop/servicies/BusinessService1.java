package sk.seidl.spring_aop.servicies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.seidl.spring_aop.repositories.Dao1;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Service
public class BusinessService1 {
    @Autowired
    private Dao1 dao;

    public String calculateSomething() {
        return dao.retrieveSomething();
    }
}
