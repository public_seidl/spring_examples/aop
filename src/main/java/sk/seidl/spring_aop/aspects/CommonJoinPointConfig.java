package sk.seidl.spring_aop.aspects;

import org.aspectj.lang.annotation.Pointcut;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
public abstract class CommonJoinPointConfig {
    @Pointcut( value = "execution(* sk.seidl.spring_aop..*.*(..))")
    public  void repositoriesPerformaceExecution(){}

    @Pointcut( value = "execution(* sk.seidl.spring_aop.servicies.*.*(..))")
    public  void serviciesPerformaceExecution(){}

    @Pointcut(value =
            "sk.seidl.spring_aop.aspects.CommonJoinPointConfig.serviciesPerformaceExecution()" +
            "&&" +
            "sk.seidl.spring_aop.aspects.CommonJoinPointConfig.repositoriesPerformaceExecution()")
    public  void allPerformanceExecution(){}

    @Pointcut("within(sk.seidl.spring_aop.repositories..*)")
    public void repositoriesPerformanceWithWithin(){}


    @Pointcut(value = "@annotation( sk.seidl.spring_aop.anotations.TractTime)")
    public void tractTimeAnnotation(){}


}
