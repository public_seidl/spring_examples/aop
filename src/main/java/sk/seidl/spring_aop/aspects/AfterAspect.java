package sk.seidl.spring_aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */

@Slf4j
@Configuration
@Aspect
public class AfterAspect {

    //execution(* PACKAGE.*.*(..))
    @AfterReturning(
            value = "execution(* sk.seidl.spring_aop.servicies.*.*(..))",
            returning = "result"
    )
    //Method name is not really important
    public void afterReturning(JoinPoint joinPoint, Object result) {
        log.error("{} returned with value {}", joinPoint, result);
    }

    @After(value = "execution(* sk.seidl.spring_aop.servicies.*.*(..))")
    //Method name is not really important
    public void after (JoinPoint joinPoint) {
        log.error(" after execution of {}", joinPoint);
    }

    @AfterThrowing(value = "execution(* sk.seidl.spring_aop.servicies.*.*(..))",
            throwing="exception")
    //Method name is not really important
    public void afterThrowing(JoinPoint joinPoint, Exception exception) {
        log.error("{} throw exception  {}", joinPoint, exception);
    }
}
