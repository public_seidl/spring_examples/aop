package sk.seidl.spring_aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */

@Slf4j
@Configuration
@Aspect
public class BeforeAspect {

    //execution(* PACKAGE.*.*(..))
    @Before("execution(* sk.seidl.spring_aop.servicies.*.*(..))")
    public void before(JoinPoint joinPoint){
        log.error("intercepted method calls-{}",joinPoint);
    }
}
