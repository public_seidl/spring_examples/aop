package sk.seidl.spring_aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Slf4j
@Aspect
@Configuration
public class MethodExecutionCalculationAspect {

    @Around("sk.seidl.spring_aop.aspects.CommonJoinPointConfig.tractTimeAnnotation()" )
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        log.warn("Time taken by {} is {}", joinPoint, timeTaken);

    }
}
