package sk.seidl.spring_aop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sk.seidl.spring_aop.servicies.BusinessService1;
import sk.seidl.spring_aop.servicies.BusinessService2;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-26
 */
@Component
@Slf4j
public class Bootstrap implements CommandLineRunner {

    @Autowired
    private BusinessService1 service1;
    @Autowired
    private BusinessService2 service2;

    @Override
    public void run(String... args) throws Exception {
        log.info("Bootstrap: run ()");
        log.info(service1.calculateSomething());
        log.info(service2.calculateSomething());
    }
}
